<?php

    // URL PARA A VERIFICAÇÃO DA ASSINATURA CMS
    const URLVerificacao = 'https://fw2.bry.com.br/api/cms-verification-service/v1/signatures/verify';
    // TOKEN AUTHORIZATION GERADO NO BRY CLOUD
    const token = "tokenDeAutenticacao";

    // VERIFICA ASSINATURA DO TIPO ATTACHED
    function verificaAssinaturaCADESAttached() {

        // CAMINHO ONDE ESTÁ LOCALIZADO O ARQUIVO A SER VERIFICADO
        $caminhoDoArquivoP7S = '/caminho/para/a/assinatura.p7s';

        $curlVerificacao = curl_init();

        curl_setopt_array($curlVerificacao, array(
        CURLOPT_URL => URLVerificacao,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('nonce ' => '1',
        'contentsReturn' => 'false',
        'signatures[0][nonce]' => '1',
        'signatures[0][content] '=> new CURLFILE($caminhoDoArquivoP7S)),
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer " . token
        ),
        ));

        // ENVIA A REQUISIÇÃO
        $respostaVerificacao = curl_exec($curlVerificacao);
        $httpcode = curl_getinfo($curlVerificacao, CURLINFO_HTTP_CODE);
        $respostaJson = json_decode($respostaVerificacao);
        curl_close($curlVerificacao);

        if($httpcode == 200) {
            echo "RESPOSTA DA VERIFICAÇÃO:\n\n";
            echo $respostaVerificacao;
            echo "\n\n__________________________________________________\n\n";
            imprimeResposta($respostaJson);
        } else {
            echo $respostaVerificacao;
        }
    }

    function verificaAssinaturaCADESDetached() {

        // CAMINHO ONDE ESTÁ LOCALIZADO A ASSINATURA A SER VERIFICADA
        $caminhoDoArquivoP7S = '/caminho/para/a/assinatura.p7s';

        // CAMINHO ONDE ESTÁ LOCALIZADO O DOCUMENTO ORIGINAL A SER VERIFICADO
        $caminhoDoDocumentoOriginal = '/caminho/para/o/documento/original.txt';

        $curlVerificacao = curl_init();

        curl_setopt_array($curlVerificacao, array(
        CURLOPT_URL => URLVerificacao,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('nonce ' => '1',
        'contentsReturn' => 'false',
        'signatures[0][nonce]' => '1',
        'signatures[0][content] '=> new CURLFILE($caminhoDoArquivoP7S),
        'signatures[0][documentContent] '=> new CURLFILE($caminhoDoDocumentoOriginal)),
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer " . token
        ),
        ));

        // ENVIA A REQUISIÇÃO
        $respostaVerificacao = curl_exec($curlVerificacao);
        $httpcode = curl_getinfo($curlVerificacao, CURLINFO_HTTP_CODE);
        $respostaJson = json_decode($respostaVerificacao);
        curl_close($curlVerificacao);

        if($httpcode == 200) {
            echo "RESPOSTA DA VERIFICAÇÃO:\n\n";
            echo $respostaVerificacao;
            echo "\n\n__________________________________________________\n\n";
            imprimeResposta($respostaJson);
        } else {
            echo $respostaVerificacao;
        }
    }

    function verificaAssinaturaCADESDetachedEnviandoHash() {

        // CAMINHO ONDE ESTÁ LOCALIZADO A ASSINATURA A SER VERIFICADA
        $caminhoDoArquivoP7S = '/caminho/para/a/assinatura.p7s';
        // HASH DO DOCUMENTO ORIGINAL QUE FOI ASSINADO
        $hashDoDocumentoOriginal = 'hashDoDocumentoOriginal';

        $curlVerificacao = curl_init();

        curl_setopt_array($curlVerificacao, array(
        CURLOPT_URL => URLVerificacao,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array('nonce ' => '1',
        'contentsReturn' => 'false',
        'signatures[0][nonce]' => '1',
        'signatures[0][content] '=> new CURLFILE($caminhoDoArquivoP7S),
        'signatures[0][documentHashes][0]' => $hashDoDocumentoOriginal),
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer " . token
        ),
        ));

        // ENVIA A REQUISIÇÃO
        $respostaVerificacao = curl_exec($curlVerificacao);
        $httpcode = curl_getinfo($curlVerificacao, CURLINFO_HTTP_CODE);
        $respostaJson = json_decode($respostaVerificacao);
        curl_close($curlVerificacao);

        if($httpcode == 200) {
            echo "RESPOSTA DA VERIFICAÇÃO:\n\n";
            echo $respostaVerificacao;
            echo "\n\n__________________________________________________\n\n";
            imprimeResposta($respostaJson);
        } else {
            echo $respostaVerificacao;
        }
    }

    function imprimeResposta($respostaJson) {

        $numeroDeAssinaturasVerificadas = count($respostaJson->verificationStatus);

        echo "Número de assinaturas verificadas: " . $numeroDeAssinaturasVerificadas;

        for ($i = 0; $i < $numeroDeAssinaturasVerificadas; $i++) {
            $assinaturaVerificada = $respostaJson->verificationStatus[$i];

            echo "\n\n";
            echo "Status geral: " . $assinaturaVerificada->generalStatus . "\n";
            echo "Formato de assinatura: " . $assinaturaVerificada->signatureFormat . "\n";

            $statusAssinatura = $assinaturaVerificada->signatureStatus[0];

            echo "Status da politica de assinatura: " . $statusAssinatura->status . "\n";
            echo "Hash Base64 original: " . $statusAssinatura->originalFileBase64Hash . "\n";
            echo "Hora da assinatura: " . $statusAssinatura->signingTime . "\n";
            echo "Referência de verificação: " . $statusAssinatura->verificationReferenceType . "\n";
            
            if(property_exists($statusAssinatura, 'signatureTimeStampStatus')) {
            $statusCarimboDoTempo = $statusAssinatura->signatureTimeStampStatus;
            if (count($statusCarimboDoTempo) > 0) {
                echo "\n";
                echo "Informações sobre o carimbo do tempo";
                echo "\n";

                $indexDoCertificadoCarimbo = count($statusCarimboDoTempo[0]->timestampChainStatus->certificateStatusList) -1;
                $certificadoCarimboDoTempo = $statusCarimboDoTempo[0]->timestampChainStatus->certificateStatusList->indexDoCertificadoCarimbo;

                if ($certificadoCarimboDoTempo != null) {
                echo "Carimbo do tempo: " . $certificadoCarimboDoTempo->certificateInfo->subjectDN->cn . "\n";
                }

                echo "Status do carimbo do tempo: " . $statusCarimboDoTempo[0]->status . "\n";
                echo "Data do carimbo do tempo: " . $statusCarimboDoTempo[0]->timeStampDate . "\n";
                echo "Data de referência da verificação: " . $statusCarimboDoTempo[0]->verificationReference . "\n";
                echo "Tipo de referência da verificação: " . $statusCarimboDoTempo[0]->verificationReferenceType . "\n";
                echo "Politica do carimbo do tempo: " . $statusCarimboDoTempo[0]->timeStampPolicy . "\n";
                echo "Hash do carimbo do tempo: " . $statusCarimboDoTempo[0]->timeStampContentHash . "\n";
                echo "Numero de serial do carimbo do tempo: " . $statusCarimboDoTempo[0]->timestampSerialNumber . "\n";
            }
            } else {
            echo " ";
            }

            if (property_exists($statusAssinatura, 'chainStatus')) {
            $statusDaCadeiaDeAssinante = $statusAssinatura->chainStatus;
            $indexCertificado = count($statusDaCadeiaDeAssinante->certificateStatusList) -1;
            $certificado = $statusDaCadeiaDeAssinante->certificateStatusList[$indexCertificado];

            echo "Informações do assinante: \n";
            echo "Assinante: " . $certificado->certificateInfo->subjectDN->cn . "\n";
            echo "Status da cadeia de certificados do assinante: " . $statusDaCadeiaDeAssinante->status. "\n";
            echo "Status do certificado do assinante: " . $certificado->status . "\n";
            echo "Certificado ICP/Brasil: " . $certificado->pkiBrazil . "\n";
            echo "Data de validade inicial: " . $certificado->certificateInfo->validity->notBefore . "\n";
            echo "Data de validade inicial: " . $certificado->certificateInfo->validity->notAfter . "\n";
            } else {
            echo "Cadeia de assinantes imcompleta: Não foi possivel verificar o certificado do assinante.";
            }

        }
    }

    verificaAssinaturaCADESAttached();
    verificaAssinaturaCADESDetached();
    verificaAssinaturaCADESDetachedEnviandoHash();

?>
    